// 10 Print Stamp

// ###################################

// Number of columns
MATRIX_COLS = 4;
// Number of rows
MATRIX_ROWS = 6;

// Units in mm
TILE_SIZE = 10;
// Units in mm
TILE_HEIGHT = 3;
TILE_HYPOTENUSE = sqrt(TILE_SIZE * TILE_SIZE + TILE_SIZE * TILE_SIZE);

// Units in mm
SEGMENT_WIDTH = 2;
// Units in mm
SEGMENT_HEIGHT = 3;

if(SEGMENT_HEIGHT <= 0) {
	assert(false, "WARNING: SEGMENT_HEIGHT must be greater than 0");
}

// ###################################

// Tile module
module tile(flip) {
	union() {
		// Base
		color("white")
		cube([TILE_SIZE, TILE_SIZE, TILE_HEIGHT], center = true);

		// Segment
		translate([0, 0, SEGMENT_HEIGHT/2 + TILE_HEIGHT/2]) {
			if (flip) {
				rotate([0, 0, 45])
					color("blue")
					cube([TILE_HYPOTENUSE+SEGMENT_WIDTH, SEGMENT_WIDTH, SEGMENT_HEIGHT], center = true);
			} else {
				rotate([0, 0, -45])
					color("red")
					cube([TILE_HYPOTENUSE+SEGMENT_WIDTH, SEGMENT_WIDTH, SEGMENT_HEIGHT], center = true);
			}
		}
	}
}

// Matrix module
module matrix() {
	union() {
		for ( i=[0:MATRIX_COLS-1]) {
			for ( j=[0:MATRIX_ROWS-1]) {
				randFlip = rands(0,1,1)[0];
				translate([i*TILE_SIZE, j*TILE_SIZE, TILE_HEIGHT/2])
				tile(round(randFlip));
				
			}
		}
	}
}

// Stamp module
module stamp() {
	difference() {
		matrix();

		// To remove segments excess
		difference() {
			translate([-TILE_SIZE/2 - SEGMENT_WIDTH, -TILE_SIZE/2 - SEGMENT_WIDTH, -0.5])
			color("green", 0.10) 
			cube([TILE_SIZE*MATRIX_COLS + SEGMENT_WIDTH*2, TILE_SIZE*MATRIX_ROWS + SEGMENT_WIDTH*2, TILE_HEIGHT+SEGMENT_HEIGHT + 1]);

			translate([-TILE_SIZE/2, -TILE_SIZE/2, -1])
			color("orange", 0.10) 
			cube([TILE_SIZE*MATRIX_COLS, TILE_SIZE*MATRIX_ROWS, TILE_HEIGHT+SEGMENT_HEIGHT + 2]);
		}
	}
}

// tile();
// matrix();
stamp();

